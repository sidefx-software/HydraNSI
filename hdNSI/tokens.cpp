#include "pxr/imaging/hdNSI/tokens.h"

PXR_NAMESPACE_OPEN_SCOPE

TF_DEFINE_PUBLIC_TOKENS(HdNSIRenderSettingsTokens, HDNSI_SETTINGS_TOKENS);

PXR_NAMESPACE_CLOSE_SCOPE
// vim: set softtabstop=4 expandtab shiftwidth=4:
